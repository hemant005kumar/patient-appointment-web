angular.module('sbAdminApp').controller('loginCtrl', function($rootScope, $http,httpService,$scope, $window, $state) {

    // scope = $scope;

$scope.signPatient=function(signup){
    console.log("Getting Patient Response=====",signup);
   
    console.log("Before processing Dateof birth===",signup.dateofbirth);
        var dob=new Date(signup.dateofbirth).toISOString();
        signup.dateofbirth=dob;
            httpService.securePost('SignUpPatient',signup)
                .success(function(resp){
                    if (resp.status=='success'){

                         swal('Success','You have registerd successfully','success');

                    }else{
                        swal('Error','Something went wrong','error');
                    }
                   
                })
                .error(function(message){
                    alert("something went wrong");

                    swal('Error',message,'error')
                })
}

    

    $scope.doLogin = function() {
        console.log("login details============",$scope.userData);

        // $http.post("http://localhost:3000/loginPatient", $scope.userData)
        httpService.securePost('loginPatient',$scope.userData)

            .success(function(response) {
                console.log("res=============",response);
                console.log("Token Response=",response.token);
                $rootScope.user = response.user;
                console.log("$rootScope.UserID====",$rootScope.user._id);
                 $rootScope.patientId=$rootScope.user._id;
                console.log("id from login ====================",$rootScope.patientId);
                $window.localStorage.setItem("user", JSON.stringify($rootScope.user));
                $window.localStorage.setItem("userId",$rootScope.user._id);
                $window.localStorage.setItem("userName",$rootScope.user.username);
                $window.localStorage.setItem("x-access-token", response.token);
                $window.localStorage.setItem("xKey", $rootScope.user.email);
                $state.go("dashboard.home");
            })
            .error(function(err) {
              console.log("error in login");
                alert("Error!", err.message, "error");
                // $scope.errorMessage = err.message;
            });

           

    };

})
.directive('onlyLettersInput', function(){
  
      return {
          restrict:'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/[^a-zA-Z]/g, ' ');
            //console.log(transformedInput);
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    });