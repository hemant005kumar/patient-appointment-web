angular.module('sbAdminApp').controller('patientProfileController', function($rootScope, $http,httpService,$scope, $window, $state,$stateParams) {

// console.log("Getting User ID This Controller==",$rootScope.userId);
$scope.userId=$rootScope.userId;
// $state.go("dashboard.patientProfile",{patientId:$scope.userId});
console.log("userID=====================",$scope.userId);
console.log("Locally User ID-",$scope.userId);


		$scope.updatePatient=function(pat){

	// var dob=pat.dateofbirth;
			console.log("dob==========================Before===",pat.dateofbirth);
			//  // pat.dateofbirth=moment(dob).format('MMMM Do YYYY, h:mm:ss a');
			//  pat.dateofbirth=dob+'T07:00:00.000Z';
			//  console.log("pat========dateofbirth",pat.dateofbirth);
			//  httpService.securePut('doc/secure/updatePatient/'+$scope.userId,pat)



			var dob=new Date(pat.dateofbirth).toISOString();



			console.log("dob==========================",dob);
		
			pat.dateofbirth=dob;
			 console.log("pat========dateofbirth",pat.dateofbirth);
			 httpService.securePut('doc/secure/updatePatient/'+$scope.userId,pat)


				console.log("Patient Information====",pat);
				console.log("Patient ID===",$scope.userId);

			 httpService.securePut('doc/secure/updatePatient/'+$scope.userId,pat)


		            .success(function(response) {
		                // console.log("res=============",response);
		                console.log("Updatting Information===",response.doc);
		                 swal('Success','Information Updated Successfully','success');
		                $scope.getPatient();
		               


		               

		                
		            })
		            .error(function(err) {
		              console.log("Something Went Wrong");
		                alert("Error!", err.message, "error");
		            });
			
			}
			$scope.getPatient=function()
			{
				httpService.secureGet('doc/secure/getPatientByID/'+$scope.userId)
				.success(function(resp){
					$scope.data=resp.doc;			
					var dob=new Date($scope.data.dateofbirth).toISOString().split('T')[0];
					$scope.data.dateofbirth=dob;
					$scope.pat=$scope.data;
				})
				.error(function(response){
					 console.log("Something Went Wrong");
		                alert("Error!", response.message, "error");

				});
			}
			$scope.getPatient();

})

.directive('onlyLettersInput', function(){
  
      return {
          restrict:'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/[^a-zA-Z]/g, ' ');
            //console.log(transformedInput);
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    });