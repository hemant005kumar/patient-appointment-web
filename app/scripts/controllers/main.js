'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
   .controller('MainCtrl', function($scope,$position,$rootScope,$state,$window) {
	    $scope.$on('$viewContentLoaded', function() { 
        $scope.userId = $rootScope.userId;
        $rootScope.authenticate();

        var xToken = $window.sessionStorage.getItem("x-access-token");
        $rootScope.xToken = JSON.parse(xToken);



    });
       
    

  });


 