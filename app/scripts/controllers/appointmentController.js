angular.module('sbAdminApp').controller('appointmentController', function($rootScope, $http,httpService,$scope, $window, $state,$stateParams) {

	$scope.btnsun= false;

	$rootScope.patientId=$window.localStorage.getItem("userId");
	$scope.getAllHospital = function()
	{
		httpService.secureGet("doc/secure/getAllHospital/all")
		.success(function(response){
			console.log("Hospital=================",response);
			$scope.hospitals = response.hospitals;
		})
	}
	$scope.getAllHospital();
	$scope.getDoctor=function()
	{
		httpService.secureGet('doc/secure/getAllDoctor/all')
		.success(function mySuccess(response){
			console.log("doctor=================",response);
			$scope.doctorList=response.doctors;
		}, function myError(response) {

			alert("something went wrong");
		});
	}
	$scope.getDoctor();
	$scope.getValue=function(app){
		httpService.secureGet("doc/secure/getHospitalByID/"+$scope.app.hospitalId)
		.success(function(response){
			$scope.hospitalId=response.doc.name;
		});

		httpService.secureGet("doc/secure/getDoctorByID/"+$scope.app.doctorId)
		.success(function(response){
			$scope.doctorId=response.doc.name;
		});
		// slotsUtility();
	}
	function slotsUtility()
	{
		httpService.secureGet("doc/secure/getScheduleByDoctorAndHospital" +"/" + $scope.app.doctorId +"/"+ $scope.app.hospitalId )
			.success(function(response)
			{
				$scope.appointment=true;
			// {
			// 	$rootScope.doctorId=response.doc.doctorId;
			// 	$rootScope.hospitalId=response.doc.hospitalId;
			// 	$rootScope.scheduleId=response.doc._id;
			// 	httpService.secureGet("doc/secure/getSlot" +"/" + $rootScope.doctorId +"/"+ $rootScope.hospitalId)
			// 	.success(function(res)
			// 	{
			// 		console.log("Slot res===",res);
			// 		if(res.doc.length==0)   
			// 		{
			// 			$scope.appointment ={
			// 				doctorId: $rootScope.doctorId,
			// 				hospitalId:$rootScope.hospitalId,
			// 				scheduleId:$rootScope.scheduleId,
			// 				days : {   
			// 					Sunday      : [],
			// 					Monday      : [],
			// 					Tuesday     : [],   
			// 					Wednesday   : [],  
			// 					Thursday    : [],   
			// 					Friday      : [],
			// 					Saturday    : []        
			// 				}

			// 			}   
			// 			for(var i=0;i<response.doc.days.Sunday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Sunday[i].start;
			// 				var end=response.doc.days.Sunday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Sunday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Monday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Monday[i].start;
			// 				var end=response.doc.days.Monday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Monday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Tuesday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Tuesday[i].start;
			// 				var end=response.doc.days.Tuesday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Tuesday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Wednesday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Wednesday[i].start;
			// 				var end=response.doc.days.Wednesday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Wednesday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Thursday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Thursday[i].start;
			// 				var end=response.doc.days.Thursday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Thursday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Friday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Friday[i].start;
			// 				var end=response.doc.days.Friday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Friday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			for(var i=0;i<response.doc.days.Saturday.length;i++)
			// 			{
			// 				var cnt=0;
			// 				var start=response.doc.days.Saturday[i].start;
			// 				var end=response.doc.days.Saturday[i].end;
			// 				var s=start.split(":");
			// 				var e=end.split(":");
			// 				var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 				var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 				for(var j=st;j<se;)
			// 				{
			// 					var n=parseFloat(j).toFixed(2);
			// 					var last=n.toString().split(".")[1];
			// 					var n1=n.split(".");
			// 					var n2=n1[0]+":"+n1[1];
			// 					var final={};
			// 					final.start=n2;
			// 					final.flag=0;
			// 					j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 					if(last==45)
			// 					{
			// 						var l=n.split(".")[0];
			// 						j=parseInt(l)+1;
			// 					}
			// 					var n=n2.split(":");
			// 					if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 					{
			// 						var t=n[0]+":"+(parseInt(n[1])+15);
			// 						final.end=t;
			// 					}
			// 					else
			// 					{
			// 						var t=(parseInt(n[0])+1)+":00";
			// 						final.end=t;
			// 					}
			// 					final.index=cnt;
			// 					$scope.appointment.days.Saturday.push(final);
			// 					cnt++;

			// 				}
			// 			}
			// 			httpService.securePost("doc/secure/createSlot",$scope.appointment)
			// 			.success(function(response){
			// 				$rootScope.slotId=response.doc._id;
			// 			})
			// 			.error(function(error){
			// 				console.log(error);
			// 			})
			// 		}
			// 		else
			// 		{
			// 			console.log("called else part===");
			// 			$scope.appointment =res.doc[0];
			// 			$rootScope.slotId=res.doc[0]._id;
			// 			httpService.secureGet("doc/secure/getScheduleByDoctorAndHospital" +"/" + $scope.app.doctorId +"/"+ $scope.app.hospitalId )
			// 			.success(function(response)
			// 			{
			// 				$scope.appointment ={
			// 					doctorId: $rootScope.doctorId,
			// 					hospitalId:$rootScope.hospitalId,
			// 					scheduleId:$rootScope.scheduleId,
			// 					days : {    
			// 						Sunday      : [],
			// 						Monday      : [],
			// 						Tuesday     : [],   
			// 						Wednesday   : [],  
			// 						Thursday    : [],   
			// 						Friday      : [],
			// 						Saturday    : []        
			// 					}
			// 				}   
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Sunday.length;i++)
			// 				{

			// 					var start=response.doc.days.Sunday[i].start;
			// 					var end=response.doc.days.Sunday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Sunday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Sunday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Sunday[k].start;
			// 								final.end=res.doc[0].days.Sunday[k].end;
			// 								final.flag=res.doc[0].days.Sunday[k].flag;
			// 								final.index=res.doc[0].days.Sunday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Sunday.push(final);
			// 					}
			// 				}
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Monday.length;i++)
			// 				{

			// 					var start=response.doc.days.Monday[i].start;
			// 					var end=response.doc.days.Monday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Monday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Monday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Monday[k].start;
			// 								final.end=res.doc[0].days.Monday[k].end;
			// 								final.flag=res.doc[0].days.Monday[k].flag;
			// 								final.index=res.doc[0].days.Monday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Monday.push(final);
			// 					}
			// 				}
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Tuesday.length;i++)
			// 				{

			// 					var start=response.doc.days.Tuesday[i].start;
			// 					var end=response.doc.days.Tuesday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Tuesday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Tuesday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Tuesday[k].start;
			// 								final.end=res.doc[0].days.Tuesday[k].end;
			// 								final.flag=res.doc[0].days.Tuesday[k].flag;
			// 								final.index=res.doc[0].days.Tuesday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Tuesday.push(final);
			// 					}
			// 				}
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Wednesday.length;i++)
			// 				{

			// 					var start=response.doc.days.Wednesday[i].start;
			// 					var end=response.doc.days.Wednesday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Wednesday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Wednesday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Wednesday[k].start;
			// 								final.end=res.doc[0].days.Wednesday[k].end;
			// 								final.flag=res.doc[0].days.Wednesday[k].flag;
			// 								final.index=res.doc[0].days.Wednesday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Wednesday.push(final);
			// 					}
			// 				}
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Thursday.length;i++)
			// 				{

			// 					var start=response.doc.days.Thursday[i].start;
			// 					var end=response.doc.days.Thursday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Thursday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Thursday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Thursday[k].start;
			// 								final.end=res.doc[0].days.Thursday[k].end;
			// 								final.flag=res.doc[0].days.Thursday[k].flag;
			// 								final.index=res.doc[0].days.Thursday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Thursday.push(final);
			// 					}
			// 				}
			// 				var cnt=0;
			// 				for(var i=0;i<response.doc.days.Friday.length;i++)
			// 				{

			// 					var start=response.doc.days.Friday[i].start;
			// 					var end=response.doc.days.Friday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Friday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Friday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Friday[k].start;
			// 								final.end=res.doc[0].days.Friday[k].end;
			// 								final.flag=res.doc[0].days.Friday[k].flag;
			// 								final.index=res.doc[0].days.Friday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Friday.push(final);
			// 					}
			// 				}
			// 				for(var i=0;i<response.doc.days.Saturday.length;i++)
			// 				{

			// 					var start=response.doc.days.Saturday[i].start;
			// 					var end=response.doc.days.Saturday[i].end;
			// 					var s=start.split(":");
			// 					var e=end.split(":");
			// 					var st=parseFloat(s[0])+parseFloat(0+"."+s[1]);
			// 					var se=parseFloat(e[0])+parseFloat(0+"."+e[1]);
			// 					for(var j=st;j<se;)
			// 					{
			// 						var n=parseFloat(j).toFixed(2);
			// 						var last=n.toString().split(".")[1];
			// 						var n1=n.split(".");
			// 						var n2=n1[0]+":"+n1[1];
			// 						var final={};
			// 						final.start=n2;
			// 						final.flag=0;
			// 						j=parseFloat((parseFloat(n)+0.15).toString()).toFixed(2);
			// 						if(last==45)
			// 						{
			// 							var l=n.split(".")[0];
			// 							j=parseInt(l)+1;
			// 						}
			// 						var final={};
			// 						var f=0;
									
			// 						for(var k=0;k<res.doc[0].days.Saturday.length;k++)
			// 						{
			// 							if(n2==res.doc[0].days.Saturday[k].start)
			// 							{
			// 								f=1;
			// 								final.start=res.doc[0].days.Saturday[k].start;
			// 								final.end=res.doc[0].days.Saturday[k].end;
			// 								final.flag=res.doc[0].days.Saturday[k].flag;
			// 								final.index=res.doc[0].days.Saturday[k].index;
			// 								cnt++;
			// 							}

			// 						}
			// 						if(f==0)
			// 						{
			// 							final.start=n2;
			// 							var n=n2.split(":");
			// 							if(n[1]=="00"||n[1]=="15"||n[1]=="30")
			// 							{
			// 								var t=n[0]+":"+(parseInt(n[1])+15);
			// 								final.end=t;
			// 							}
			// 							else
			// 							{
			// 								var t=(parseInt(n[0])+1)+":00";
			// 								final.end=t;
			// 							}
			// 							final.index=cnt;
			// 							final.flag=0;
			// 							cnt++;
			// 						}
			// 						$scope.appointment.days.Saturday.push(final);
			// 					}
			// 				}
			// 				httpService.securePut("doc/secure/updateSlot/"+$rootScope.slotId,$scope.appointment)
			// 				.success(function(response){
			// 					console.log("updated slots");
			// 				})
			// 				.error(function(error){
			// 					console.log(error);
			// 				})

			// 			});
			// 		}
			// 	});
		});
	}
$scope.app_values = function(s,index,dayN){
	$rootScope.start=s;
	$rootScope.app_date=0;
	$rootScope.dayN=dayN;
	$rootScope.index=index;
	var n=s.split(":");
	if(n[1]=="00"||n[1]=="15"||n[1]=="30")
	{
		var t=n[0]+":"+(parseInt(n[1])+15);
		$rootScope.end=t;
	}
	else
	{
		var t=(parseInt(n[0])+1)+":00";
		$rootScope.end=t;
	}
	var curr = new Date; 
	var Sun = curr.getDate() - curr.getDay();
	var Mon = Sun+1;
	var Tues = Sun +2;
	var Wed = Sun +3;
	var Thur = Sun +4;
	var Fri = Sun +5;
	var Sat = Sun +6;;
	if($rootScope.dayN===0)
	{
		var Sunday = new Date(curr.setDate(Sun)).toUTCString();
		
		date = new Date(Sunday);
		year = date.getFullYear();
		month = date.getMonth()+1;
		dt = date.getDate();

		if (dt < 10) 
		{
			dt = '0' + dt;
		}
		if (month < 10) 
		{
			month = '0' + month;
		}
		$rootScope.app_date = year+'-'+month+'-'+dt;
	}
	else if($rootScope.dayN===1){

		var Monday = new Date(curr.setDate(Mon)).toUTCString();
		
		date = new Date(Monday);
		year = date.getFullYear();
		month = date.getMonth()+1;
		dt = date.getDate();

		if (dt < 10) 
		{
			dt = '0' + dt;
		}
		if (month < 10) 
		{
			month = '0' + month;
		}
		$rootScope.app_date = year+'-'+month+'-'+dt;

	}else if($rootScope.dayN===2){
		var Tuesday=new Date(curr.setDate(Tues)).toUTCString();
		date=new Date(Tuesday);
		year=date.getFullYear();
		month=date.getMonth()+1;
		dt=date.getDate();
		if(dt<10){
			dt='0'+dt;
		}
		if(month<10){

			month= '0'+month;
		}

		$rootScope.app_date= year +'-'+month+'-'+dt;

	}else if($rootScope.dayN===3){

		var Wednesday=new Date(curr.setDate(Wed)).toUTCString();
		date=new Date(Wednesday);
		year=date.getFullYear();
		month=date.getMonth()+1;
		dt=date.getDate();
		if(dt<10){
			dt='0'+dt;
		}
		if(month<10){

			month= '0'+month;
		}
		$rootScope.app_date = year +'-'+month+'-'+dt;

	}else if($rootScope.dayN===4){

		var Thursday=new Date(curr.setDate(Thur)).toUTCString();
		date=new Date(Thursday);
		year=date.getFullYear();
		month=date.getMonth()+1;
		dt=date.getDate();
		if(dt<10){
			dt='0'+dt;
		}
		if(month<10){

			month= '0'+month;
		}
		$rootScope.app_date = year +'-'+month+'-'+dt;

	}else if($rootScope.dayN===5){

		var Friday=new Date(curr.setDate(Fri)).toUTCString();
		date=new Date(Friday);
		year=date.getFullYear();
		month=date.getMonth()+1;
		dt=date.getDate();
		if(dt<10){
			dt='0'+dt;
		}
		if(month<10){

			month= '0'+month;
		}
		$rootScope.app_date = year +'-'+month+'-'+dt;

	}else if($rootScope.dayN===6){

		var Saturday=new Date(curr.setDate(Sat)).toUTCString();
		date=new Date(Saturday);
		year=date.getFullYear();
		month=date.getMonth()+1;
		dt=date.getDate();
		if(dt<10){
			dt='0'+dt;
		}
		if(month<10){

			month= '0'+month;
		}
		$rootScope.app_date = year +'-'+month+'-'+dt;

	}
}


$scope.postAppointment=function(app,start,end){
	$scope.app.patientId=$scope.patientId;
	$scope.app.slotId=$rootScope.slotId;
	$scope.app.start=start;
	$scope.app.end=end;
	$scope.app.status="waiting";
	var newdate=new Date($rootScope.app_date).toISOString();
	$scope.app.date=newdate;
	httpService.securePost('doc/secure/createAppointment',$scope.app)
	.success(function(response){
		var day=$rootScope.dayN;
		if(day==0)
		{
			$scope.appointment.days.Sunday[$rootScope.index].flag=1;
		}
		else if(day==1)
		{
			$scope.appointment.days.Monday[$rootScope.index].flag=1;
		}
		else if(day==2)
		{
			$scope.appointment.days.Tuesday[$rootScope.index].flag=1;
		}
		else if(day==3)
		{
			$scope.appointment.days.Wednesday[$rootScope.index].flag=1;
		}
		else if(day==4)
		{
			$scope.appointment.days.Thursday[$rootScope.index].flag=1;
		}
		else if(day==5)
		{
			$scope.appointment.days.Friday[$rootScope.index].flag=1;
		}
		else if(day==6)
		{
			$scope.appointment.days.Saturday[$rootScope.index].flag=1;
		}
		
		httpService.securePut('doc/secure/updateSlot/'+$rootScope.slotId,$scope.appointment)
		.success(function(response){
			console.log("done====");
			slotsUtility();
		})

		// if(response.status=='success'){

		// swal({
		// 	  title: "Success",
		// 	  text: response.message,
		// 	  type: "success",
		// 	  button: "Ok",
		// 	  timer:1000,
		// 	});
		// }
		// else if(response.status=='error'){
		// 	swal('Error!',response.message,'error');
		// }
		
	}, function myError(response) {
		alert("something went wrong");
	});
}

$scope.getAppointmentList=function(){
	httpService.secureGet("doc/secure/getAppointmentForPatient/"+$scope.patientId)
	.success(function(response) {
		$scope.data = response.appointments;
		for(var i=0;i<$scope.data.length;i++){                    
			if($scope.data[i].date)
			{
				date = new Date($scope.data[i].date);
				year = date.getFullYear();
				month = date.getMonth()+1;
				dt = date.getDate();

				if (dt < 10) {
					dt = '0' + dt;
				}
				if (month < 10) {
					month = '0' + month;
				}
				$scope.data[i].date=dt+'-'+month+'-'+year;
			}
		}   
		$scope.appointmentList = $scope.data;
	})
	.error(function(error) {
		swal("error", "error in getting units", "error")
		console.log(error);
	})
}
$scope.getAppointmentList();

});