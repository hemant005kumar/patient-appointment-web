angular.module('sbAdminApp')
.controller('reportController', function($rootScope, $http,httpService,$scope, $window, $state,$stateParams) {
	

	$scope.patientId=$rootScope.userId;
	var apid;

	
	$scope.getAppointmentList=function(){
		httpService.secureGet("doc/secure/getAppointmentForPatient/"+$scope.patientId)
		.success(function(response) {

			console.log("Updated Files=====",response);
			// console.log("File Name=====",response.appointments[0].fileName);
			var spit;
			
			// console.log("After Spliting File Name===",disp);
				$scope.data = response.appointments;
			 	for(var i=0;i<$scope.data.length;i++){                    
	      //               if($scope.data[i].date)
	      //               {
	      //               	date = new Date($scope.data[i].date);
							// year = date.getFullYear();
							// month = date.getMonth()+1;
							// dt = date.getDate();

							// if (dt < 10) {
							//   dt = '0' + dt;
							// }
							// if (month < 10) {
							//   month = '0' + month;
							// }
	      //                   $scope.data[i].date=dt+'-'+month+'-'+year;
	      			$scope.data[i].date=new Date(response.appointments[i].date).toISOString().split('T')[0];
	                    // }
	            }   
	            for(var j=0; j<$scope.data.length; j++){

	            		spit=response.appointments[j].fileName;
	            		console.log("File Name in for Loop==",response.appointments[j].fileName);
	            		
	            }

	            $scope.appointmentList = $scope.data;
		})
		.error(function(error) {
			swal("error", "error in getting units", "error")
			console.log(error);
		})
	}
	
	$scope.postPatientData=function(up){
		console.log("patient ID===",$scope.up);

		// console.log("Appointment ID====",appid);
		// $scope.up.appid=$scope.appid;
		// $scope.up.patientId=$scope.patientId;
		// console.log("Appending Data===",$scope.up);
		
		// console.log("Total Data====",$scope.patientId);
			 $scope.selectedFiles($scope.appid);
		 var newobj=JSON.stringify($scope.appid);
			var formData= new FormData(document.getElementById("uploaddoc"));
			// var file1=$('#file')[0].files[0];
			// console.log("Files===",file1);
   //    		formData.append('updoc',file1);
   //    		formData.append('ID',newobj);

			// 	formData.append('id',$scope.appid);
		
		 
		 console.log("Getting FormData===",formData);
		console.log("Appintment ID Post ===",$scope.appid);
		httpService.securePostFile("doc/secure/uploadTest/"+ $scope.appid,formData)
		.success(function(response) {

			console.log("Uploading response====",response);
			if(response.status=='success'){
				swal('Success!',response.message,'success');
				$scope.getAppointmentList();


			}else
			{
				swal('Error!','Something went wrong','error');
			}

		})
		.error(function(error) {
			swal("error", "error in getting units", "error")
			console.log(error);
		})

	}

	$scope.printDoc=function(id){
		console.log("Apponiment ID===",id);
		apid=id;

		httpService.secureGet("doc/secure/getAppointmentForPatient/"+$scope.patientId)
		.success(function(response) {

			for(var i=0;i<response.appointments.length; i++){

				if(id==response.appointments[i]._id){

					console.log("Proper Data of ID====",response.appointments[i].fileName);
					var imagesrc=response.appointments[i].fileName;
					console.log("Before Hello Image==",imagesrc);
					
						console.log("Hello Image==",imagesrc);
						window.open('http://localhost:8081/uploads/'+imagesrc);

					

				}
				

			}
			
			


		})
		.error(function(error) {
			swal("error", "error in getting units", "error")
			console.log(error);
		})

	}
	$scope.removeFileName=function(id,filename){
		console.log("Appointment ID==",id);
		console.log("FileName===",filename);
		if (filename==undefined) {
			swal({

					 title: "Sorry",
					 text: 'File Not Found',
					 type: "error",
			 		 button: "Ok",
					 timer:1000
								
			})
		}else{

					swal({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, delete it!',
					  cancelButtonText: 'No, cancel!',
					  confirmButtonClass: 'btn btn-success',
					  cancelButtonClass: 'btn btn-danger',
					  buttonsStyling: false,
					  reverseButtons: true
					}).then((result) => {
					  if (result.value) {
					  	console.log("After Confirming Deleted=====",id,"===",filename);
					    var file={filename:filename};
					    httpService.securePut('doc/secure/updateAppointmentFile/'+id,file)
							.success(function(response){
							console.log("File Remove Response====",response);
							if(response.status=='success'){
								swal({
									 title: "Success",
									 text: response.message,
							 		 type: "success",
							 		 button: "Ok",
									 timer:1000
								})
								$scope.getAppointmentList();

							}else if(response.status=='error'){
								swal({
									 title: "Error",
									 text: response.message,
							 		 type: "error",
							 		 button: "Ok",
									 timer:1000
								})

							}
					

						})
						.error(function(response){
								swal({
									 title: "Error",
									 text: 'Something went wrong',
							 		 type: "error",
							 		 button: "Ok",
									 timer:1000
								})
						})
					  // result.dismiss can be 'cancel', 'overlay',
					  // 'close', and 'timer'
					  } else if (result.dismiss === 'cancel') {
					    swal(
					      'Cancelled',
					      'Your imaginary file is safe :)',
					      'error'
					    )
					  }
					})
				}


		
		
	}
	$scope.selectedFiles=function(id){
		// $scope.filename={};
		console.log("Print Appointment ID After Uploading file==",id);
		httpService.secureGet("doc/secure/getAppointmentForPatient/"+$scope.patientId)
		.success(function(response) {
			
			console.log("File Name response====",response.appointments);
			for (var i = 0; i <response.appointments.length; i++) {
				if(id==response.appointments[i]._id){
					// $scope.filename=response.appointments[i];
					$scope.filename=response.appointments[i].fileName;
					// $scope.filename[i].original_Name=response.appointments[i].original_Name;	
					
				}
				
			}
			console.log("Output of File Name=====",$scope.filename);
			$scope.fileList=$scope.filename;
			



		})
		.error(function(error) {
			swal("error", "error in getting units", "error")
			console.log(error);
		})

	}
	$scope.postApid=function(id){
		console.log("Appointment ID==",id);
		$scope.appid=id;
	}

$scope.clear=function(){
	$scope.app.disease_description="";
}


$scope.getAppointmentList();
$scope.selectedFiles();

})


// Hook in a directive
.directive('fileDownload',function(){

  return {
    restrict: 'E', // applied on 'element'
    scope: {
      fileurl: '@fileurl',
      linktext: '@linktext',
      // furl:'http://localhost:8081/uploads/'+fileurl      
    },
    template: '<a href="{{ fileurl }}" download>{{ linktext }}</a>', // need this so that the inner HTML can be re-used
    link: function(scope, elem, attrs) {
      /* Ref: https://thinkster.io/egghead/isolate-scope-at
       * This block is used when we have
       * scope: {
           fileurl: '=fileurl',
           linktext: '=linktext'     
         }          
       scope.fileurl = attrs.fileurl;
       scope.linktext = attrs.linktext;*/
    }               
  }
})
// angular.bootstrap(document, ['sbAdminApp']);