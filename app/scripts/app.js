'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
var sbAdmin=angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar'

    // 'ngTable'
  ])
  sbAdmin.config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider) {
    
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login',{
            url:'/login',
            templateUrl:'views/login/login.html',
              controller:"loginCtrl",
              resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[
                    'scripts/controllers/loginCtrl.js'
                  // 'scripts/directives/timeline/timeline.js',
                  // 'scripts/directives/notifications/notifications.js',
                  // 'scripts/directives/chat/chat.js',
                  // 'scripts/directives/dashboard/stats/stats.js'
                  ]
                })
              }
            }
        })


     // .state('login',{
     //    url:'/login',
     //    controller: 'loginCtrl',
     //    templateUrl:'views/login/login.html',
     //    resolve: {
     //      loadMyFiles:function($ocLazyLoad) {
     //        return $ocLazyLoad.load({
     //          name:'Login',
     //          files:[
     //          'scripts/controllers/loginCtrl.js'
     //          // 'scripts/directives/timeline/timeline.js',
     //          // 'scripts/directives/notifications/notifications.js',
     //          // 'scripts/directives/chat/chat.js',
     //          // 'scripts/directives/dashboard/stats/stats.js'
     //          ]
     //        })
     //      }
     //    }
     //  })
      .state('dashboard', {
        url:'/dashboard',
        templateUrl: 'views/dashboard/main.html',
       
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[

                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-notification/header-notification.js',
                    'scripts/directives/sidebar/sidebar.js',
                    'scripts/directives/sidebar/sidebar-search/sidebar-search.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('dashboard.home',{
        url:'/home',
        controller: 'MainCtrl',
        templateUrl:'views/dashboard/home.html',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[

              'scripts/controllers/main.js',
              'scripts/directives/timeline/timeline.js',
              'scripts/directives/notifications/notifications.js',
              'scripts/directives/chat/chat.js',
              'scripts/directives/dashboard/stats/stats.js'
              ]
            })
          }
        }
      })
       .state('dashboard.patientProfile',{
            url:'/patientProfile',
            templateUrl:'views/PatientProfile/profile.html',
              controller:"patientProfileController",
              resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[
                    'scripts/controllers/patientProfileController.js'
                  // 'scripts/directives/timeline/timeline.js',
                  // 'scripts/directives/notifications/notifications.js',
                  // 'scripts/directives/chat/chat.js',
                  // 'scripts/directives/dashboard/stats/stats.js'
                  ]
                })
              }
            }
        })

       .state('dashboard.appointmants',{
            url:'/appointmants',
            templateUrl:'views/appointmants/appointmants.html',
              controller:"appointmentController",
              resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[

                    'styles/style.css',
                    'scripts/controllers/appointmentController.js'
                  // 'scripts/directives/timeline/timeline.js',
                  // 'scripts/directives/notifications/notifications.js',
                  // 'scripts/directives/chat/chat.js',
                  // 'scripts/directives/dashboard/stats/stats.js'
                  ]
                })
              }
            }
        })
       .state('dashboard.appointmentsList',{
            url:'/appointmentsList',
            templateUrl:'views/appointmants/appointmentList.html',
              controller:"appointmentController",
              resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[
                    'scripts/controllers/appointmentController.js'
                  // 'scripts/directives/timeline/timeline.js',
                  // 'scripts/directives/notifications/notifications.js',
                  // 'scripts/directives/chat/chat.js',
                  // 'scripts/directives/dashboard/stats/stats.js'
                  ]
                })
              }
            }
        })
       .state('dashboard.report',{
            url:'/report',
            templateUrl:'views/report/report.html',
              controller:"reportController",
              resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[
                    // './bower_components/bootstrap/dist/css/bootstrap.min.css',
                    './bower_components/bootstrap-fileinput/css/fileinput.css',
                    './bower_components/bootstrap-fileinput/css/fileinput-rtl.css',
                    './bower_components/bootstrap-fileinput/css/fileinput.min.css',

                     'assets/js/jquery.js',
                    './bower_components/bootstrap-fileinput/js/plugins/piexif.js',
                    './bower_components/bootstrap-fileinput/js/fileinput.js',
                    './bower_components/bootstrap-fileinput/js/fileinput.min.js',
                     'scripts/controllers/reportController.js'
                  // 'scripts/directives/timeline/timeline.js',
                  // 'scripts/directives/notifications/notifications.js',
                  // 'scripts/directives/chat/chat.js',
                  // 'scripts/directives/dashboard/stats/stats.js'
                  ]
                })
              }
            }
        })

       .state('dashboard.pescription',{
        url:'/pescription',
        templateUrl:'views/pescriptions/pescription.html',
        controller:'pescriptionController',
         resolve: {
              loadMyFiles:function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name:'sbAdminApp',
                  files:[
                      'scripts/controllers/pescriptionController.js'
                  ]
                
                })
              }
            }
        
      })
      
      
      .state('dashboard.form',{
        templateUrl:'views/form.html',
        url:'/form'
    })
      .state('dashboard.blank',{
        templateUrl:'views/pages/blank.html',
        url:'/blank'
    })
    //   .state('login',{
    //      url:'/login',
    //     templateUrl:'views/login/login.html',
    //       resolve: {
    //       loadMyFiles:function($ocLazyLoad) {
    //         return $ocLazyLoad.load({
    //           name:'sbAdminApp',
    //           files:[
    //           'scripts/controllers/loginCtrl.js'
    //           // 'scripts/directives/timeline/timeline.js',
    //           // 'scripts/directives/notifications/notifications.js',
    //           // 'scripts/directives/chat/chat.js',
    //           // 'scripts/directives/dashboard/stats/stats.js'
    //           ]
    //         })
    //       }
    //     }
    // })
      .state('dashboard.chart',{
        templateUrl:'views/chart.html',
        url:'/chart',
        controller:'ChartCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/chartContoller.js']
            })
          }
        }
    })
      .state('dashboard.table',{
        templateUrl:'views/table.html',
        url:'/table'
    })
      .state('dashboard.panels-wells',{
          templateUrl:'views/ui-elements/panels-wells.html',
          url:'/panels-wells'
      })
      .state('dashboard.buttons',{
        templateUrl:'views/ui-elements/buttons.html',
        url:'/buttons'
    })
      .state('dashboard.notifications',{
        templateUrl:'views/ui-elements/notifications.html',
        url:'/notifications'
    })
      .state('dashboard.typography',{
       templateUrl:'views/ui-elements/typography.html',
       url:'/typography'
   })
      .state('dashboard.icons',{
       templateUrl:'views/ui-elements/icons.html',
       url:'/icons'
   })
      .state('dashboard.grid',{
       templateUrl:'views/ui-elements/grid.html',
       url:'/grid'
   })
  }]);

 sbAdmin.run(function($rootScope, $state, $window) {
        $rootScope.$state = $state; // state to be accessed from view
        // $rootScope.$settings = settings; // state to be accessed from view
        $rootScope.$on("$stateChangeError", console.log.bind(console));

                $rootScope.user= $window.localStorage.getItem("user");
                $rootScope.userId=$window.localStorage.getItem("userId");
                $rootScope.userName=$window.localStorage.getItem("userName");
                $window.localStorage.getItem("x-access-token");
                $window.localStorage.getItem("xKey");
                console.log("User=",$rootScope.user);
                console.log("User ID=",$rootScope.userId);
                console.log("User Name=",$rootScope.userName);


                   $rootScope.logout = function() 
                     {
                     console.log("Hi You Have Logout");
                      $window.localStorage.clear();
                            $state.go("login");
                            // $state.reload();
                     }
       

        $rootScope.$on("$stateChangeSuccess", function(event, toState, fromState) {
            $rootScope.authenticate();
        });



        $rootScope.authenticate = function() {
            
                console.log("[lopijoiujjujjjikj");
            if ($state.current.name.split(".")[0] != "login") {
                var name=$state.current.name.split(".")[0];
                console.log("Current Name Before Use of Split=====",$state.current.name);
                console.log("Current Name=====",name);
                $rootScope.user = JSON.parse($window.localStorage.getItem("user"));

                /*if ($state.current.name == "dashboard.home") {
                    $rootScope.headerPic = true;
                } else {
                    $rootScope.headerPic = false;
                }*/

                if (!$rootScope.user) {
                    $rootScope.logout();
                    swal({
                        title:'Sorry!',
                        text:'Session has been expired',
                        type:'info',
                        timer:2000

                    })
                    return;
                }
                $rootScope.token = $window.localStorage.getItem("token");
                // $rootScope.isAdminLoggedIn = $rootScope.user.type == "admin" ? true : false;
            }

            if ($state.current.name.split(".")[0] == "dashboard") {
                $rootScope.user = JSON.parse($window.localStorage.getItem("user"));
            }
        };
});


    
